<?php

/**
 * ajouter un champ newsletter sur le formulaire CVT carte_postale
 *
 * @param array $flux
 * @return array
 */
function cpn_editer_contenu_formulaire_carte_postale($flux){
	$cpn = recuperer_fond('formulaires/inc-cpn', array('newsletter' => $flux['args']['contexte']['newsletter']));
	$flux['data'] = preg_replace('%(<li class="editer_email_expediteur(.*?)</li>)%is', '$1'."\n" . $cpn, $flux['data']);
	return $flux;
}

/**
 * charger les valeurs du champ newsletter sur le formulaire CVT carte_postale
 *
 * @param array $flux
 * @return array
 */
function cpn_formulaire_charger($flux){
	if ($flux['args']['form']=='carte_postale') {
		$flux['data']['newsletter'] = '';
	}
	return $flux;
}

/**
 * traiter la newsletter sur le formulaire CVT carte_postale
 *
 * @param array $flux
 * @return array
 */
function cpn_formulaire_traiter($flux){
	if ($flux['args']['form']=='carte_postale') {
		if (_request('newsletter') and $email = _request('email_expediteur')) {
			$nom = _request('nom_expediteur');
			$valider_abonnement = charger_fonction('cpn_valider_abonnement','inc');
			$valider_abonnement($email, $nom);
		}
	}
	return $flux;
}

function inc_cpn_valider_abonnement_dist($email, $nom='') {
	// spip_lettre
	include_spip('lettres_fonctions');
	include_spip('inc/lettres_classes'); // aucazou
	$abonne = new abonne(0, $email);
	$abonne->nom = $nom;
	$abonne->enregistrer(); // creer l'abonne si besion
	$abonne->enregistrer_abonnement(); // l'inscrire aux newsletter generales
	$abonne->valider_abonnements_en_attente(); // valider son inscription
}

?>
